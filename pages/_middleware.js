import { NextResponse } from 'next/server';


export default async function middleware(NextRequest, NextFetchEvent) {
    const userData = NextRequest.cookies?.UserLoginDataKey
    const pathName = NextRequest.nextUrl.pathname
    const baseUrl = NextRequest.nextUrl.origin



    const publicPages = ['/', '/auth/forget_password', '/auth/email_confirmation', '/auth/new_password', '/img/logo.png', '/img/login.png', '/img/forgot.png']


    if (userData && publicPages.includes(pathName)) {
        return NextResponse.redirect(baseUrl + '/dashboard')
    }

    else if (!userData && !publicPages.includes(pathName)) {
        return NextResponse.redirect(baseUrl + '/')
    }


}
