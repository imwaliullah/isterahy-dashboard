import { ToastContainer } from 'react-toastify';
import NextNProgress from "nextjs-progressbar";
import '../styles/globals.css';
import '../styles/main.scss';
import '../styles/font.css';
import 'react-toastify/dist/ReactToastify.css';

function MyApp({ Component, pageProps }) {
  return (
    <>
      <NextNProgress color="#00B55D" />
      <Component {...pageProps} />
      <ToastContainer />
    </>
  )
}

export default MyApp
