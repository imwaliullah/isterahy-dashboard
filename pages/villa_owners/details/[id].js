import React, { useEffect, useState } from 'react';
import Layout from '../../../components/Layout';
import Head from 'next/head';
import BookingModal from '../../../components/Modal/index';

import VillaInformation from '../../../components/villas/VillaInformation';
import VillaOwner from '../../../components/villas/VillaOwner';
import CurrentBookings from '../../../components/villas/CurrentBookings';
import AllBookings from '../../../components/villas/AllBookings';
import Cancelled from '../../../components/villas/Cancelled';
import Transactions from '../../../components/villas/Transactions'
import Dispute from '../../../components/villas/Dispute'
import VendorInformation from '../../../components/villa_owners/VendorInformation';
import VillasList from '../../../components/villa_owners/VillasList';
import API from '../../../utilis/API';
import getHeader from '../../../utilis/getHeader';
import Loader from '../../../components/Loader';
import { toast } from 'react-toastify';

const index = ({ vendorId }) => {
    const header = getHeader();
    const [selectedTab, setSelectedTab] = useState({});
    const [vendorDetails, setVendorDetails] = useState();

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setSelectedTab({
            id: 1,
            name: 'Vendor Information'
        })
        getVendorDetails();
    }, [])

    const getVendorDetails = async () => {
        setLoading(true)
        try {
            const { data } = await API.get(`Dashboard/vendor/detail/${vendorId}`, header);
            console.log("details", data);


            if (data?.success == true) {
                setVendorDetails(data?.body)
            }
            setLoading(false)
        } catch (error) {
            setLoading(false)
            console.log(error);
        }
    }

    const ApproveBooking = async () => {
        setLoading(true)
        try {
            const { data } = await API.get(`Dashboard/vendor/approve/account/${vendorId}`, header);
            if (data?.success == true) {
                getVendorDetails();
                toast.success('Vendor approved successfully')
            }
            setLoading(false)
        } catch (error) {
            setLoading(false)
            console.log(error);
        }
    }

    const tabs = [
        {
            id: 1,
            name: 'Vendor Information'
        },
        {
            id: 2,
            name: 'Villa Listed'
        },
    ]
    return (
        <>
            {loading && <Loader />}
            <Head>
                <title>Villa details</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Layout title="Villa Detail">
                <section className='flex justify-between items-center mt-3 pr-4'>
                    <div>
                        <h1 className='text-[24px] text-black font-medium font-sans'>Villa ID: {vendorId}</h1>
                        <p className='text-lightGray text-base py-1 font-sans'>Joining Date: {vendorDetails?.joining_date}</p>
                        <p className='text-lightGray text-base font-sans flex gap-x-3'>Status
                            {vendorDetails?.vendor?.status == 'yes' ? (
                                <>
                                    <span className='text-success font-semibold'>  Approved</span>
                                    <img src='/img/approved.svg' alt='' />
                                </>
                            ) : (
                                <span className='text-red-500 font-semibold'>  Pending</span>
                            )}
                        </p>
                    </div>
                    {vendorDetails?.vendor?.status !== 'yes' && (
                        <button className='w-[176px] h-[46px] bg-success text-white font-light text-sm rounded-[14px]' onClick={ApproveBooking}>Approve Vendor</button>
                    )}
                </section>
                <section className=''>
                    <div className='w-[431px] h-[50px] bg-primaryLight rounded-lg my-7 flex justify-start gap-x-10'>
                        {tabs.map((tab, index) => (
                            <button
                                className={`px-7 flex-1 h-full rounded-lg text-primary text-xs font-medium transition-colors duration-300 ${selectedTab.id === tab?.id && "text-white bg-primary"}`}
                                onClick={() => setSelectedTab(tab)}
                            >{tab?.name}</button>
                        ))}
                    </div>
                    {selectedTab?.name === "Vendor Information" && (
                        <VendorInformation vendor={vendorDetails?.vendor} />
                    )}
                    {selectedTab?.name === "Villa Listed" && (
                        <VillasList vendorList={vendorDetails?.villa_lists} />
                    )}

                </section>

            </Layout>
        </>
    )
}

export default index;

export const getServerSideProps = async (context) => {
    const id = context?.params?.id;
    return {
        props: { vendorId: id }
    }
}