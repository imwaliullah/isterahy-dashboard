import React, { useEffect, useState } from 'react';
import Layout from '../../components/Layout';
import Head from 'next/head';
import UserCard from '../../components/reservations/UserCard';
import Search from '../../components/SVGs/Search';
import Link from 'next/link';
import API from '../../utilis/API';
import getHeader from '../../utilis/getHeader';
import Loader from '../../components/Loader'
import PaginationElement from '../../components/PaginationElement';
import baseURL from '../../utilis/baseURL';

const index = () => {

    const header = getHeader();
    const [vendorsList, setVendorsList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [cardsData, setCardsData] = useState([]);

    // For pagination
    const [pageNumber, setPageNumber] = useState(1)
    const [lastPage, setLastPage] = useState();
    const [search, setSearch] = useState('');

    const getVendorsList = async () => {
        setLoading(true)
        const endPoint = search ? `Dashboard/vendors/all/${search}?page=${pageNumber}` : `Dashboard/vendors/all?page=${pageNumber}`
        try {
            const { data } = await API.get(endPoint, header);


            if (data?.success === true) {
                console.log("villa list: ", data?.body?.users?.data)
                setVendorsList(data?.body?.users?.data);
                setCardsData([
                    {
                        title: 'All Vendors',
                        value: data?.body?.total_user,
                        color: '#2E3039'
                    },
                    {
                        title: 'New Vendors',
                        value: data?.body?.new_users,
                        color: '#08488C'
                    },
                ])

                setLastPage(data?.body?.users?.last_page);
            }
            setLoading(false)
        } catch (error) {
            setLoading(false)
            console.log(error);
        }
    }

    useEffect(() => {
        getVendorsList();
    }, [pageNumber, search])

    return (
        <>
            {loading && <Loader />}
            <Head>
                <title>Pending Villas</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Layout title="Villa Vendors" slug="Check the Vendors villa">
                <section className='mt-5'>
                    <div className='flex gap-x-12 items-center'>
                        <p className='text-lg text-black'>Explore</p>
                        <div className='w-[448px] h-[48px] rounded-3xl border overflow-hidden flex justify-between'>
                            <input
                                type="text"
                                placeholder='Search villa by name or ID'
                                className='font-sans text-base text-lightGray h-full pl-5 flex-1 placeholder:font-light'
                                value={search}
                                onChange={(e) => setSearch(e.target.value)}
                            />
                            <button className='w-[90px] h-[48px] rounded-3xl bg-primary flex justify-center items-center'>
                                <Search />
                            </button>
                        </div>
                    </div>
                </section>
                {/* Cards  Start*/}
                <section className='w-full pb-3'>
                    <div className='flex flex-row flex-wrap space-x-[24px] mt-[29px]'>
                        {
                            cardsData?.map((item, index) => {
                                return (
                                    <div
                                        key={index + ''}
                                        className='w-[209px] h-[135px] bg-white rounded-[8px] flex flex-col pt-[22px] px-[25px]'
                                        style={{
                                            'boxShadow': '0px 0px 20px #D7DEE365'
                                        }}
                                    >
                                        <h1 className={`font-roboto text-darkBlack text-xl font-medium`}>{item?.title}</h1>
                                        <h2 className={`font-roboto font-bold text-[${item?.color}] text-2xl mt-[13px]`}>{item?.value}</h2>
                                    </div>
                                )
                            })
                        }
                    </div>
                </section>
                {/* Table start */}
                <div
                    className='bg-white py-[22px] mt-[28px] rounded-[8px] relative mb-20'
                    style={{
                        'boxShadow': '0px 0px 20px #D7DEE365'
                    }}
                >
                    <div className='px-[25px] flex justify-between items-center'>
                        <h1 className='font-medium text-[20px] text-[#2E3039]'>All VENDOR</h1>
                        <a href={`${baseURL}Dashboard/vendors/data`} download target="_blank">
                            <button className='px-5 py-2 text-sm rounded-lg text-primary font-medium border border-primary'>Download villas</button>
                        </a>
                    </div>
                    <div className='mt-[20px] overflow-x-auto md:overflow-x-hidden table_wrapper'>
                        <table className='table-fixed w-[800px] md:w-full'>
                            <thead >
                                <tr className='bg-[#08488C]'>
                                    <th className='py-[11px] text-xs text-white font-light text-start  pl-[36px] w-[200px] bg-primary'>VENDOR NAME</th>
                                    <th className='py-[11px] text-xs text-white font-light text-start bg-primary'>VENDOR ID</th>
                                    <th className='py-[11px] text-xs text-white font-light text-start w-[100px] bg-primary'>EMAIL</th>
                                    <th className='py-[11px] text-xs text-white font-light text-start pl-[25px] bg-primary'>CONTACT NO</th>
                                    <th className='py-[11px] text-xs text-white font-light text-start bg-primary'>VILLA LISTED</th>
                                    <th className='py-[11px] text-xs text-white font-light text-start bg-primary'>STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                                {vendorsList?.map((vendor, index) => {
                                    return (
                                        <Link href={`/villa_owners/details/${vendor?.id}`}>
                                            <tr className='border-b last:border-b-0 border-[#EFF3F9] cursor-pointer' key={index}>
                                                <td className=' pl-[36px] py-[24px] text-[#54617A]  text-sm font-roboto '>{vendor?.vendor_name}</td>
                                                <td className='py-[24px] text-[#54617A] font-roboto text-sm'>{vendor?.id}</td>
                                                <td className='py-[24px] text-[#54617A] font-roboto text-sm '>{vendor?.email}</td>
                                                <td className='py-[24px] pl-[25px] text-[#54617A] font-roboto text-sm'>{vendor?.phone}</td>
                                                <td className='py-[24px] text-black font-roboto text-sm'>{vendor?.total_villas}</td>
                                                <td className={`py-[24px] font-medium font-roboto text-sm ${vendor?.status == 'yes' ? "text-green-500" : "text-red-500"}`}>{vendor?.status == "yes" ? "Approved" : "Pending"}</td>
                                            </tr>
                                        </Link>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>

                    {/* Pagination */}
                    <PaginationElement
                        currentPage={pageNumber}
                        callBack={(currentPage) => { setPageNumber(currentPage) }}
                        totalPages={lastPage}
                    />
                </div>
                {/* Table end */}
            </Layout>
        </>
    )
}

export default index