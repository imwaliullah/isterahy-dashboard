import axios from "axios"

const API = axios.create({
    baseURL: 'https://www.estrahty.tardemdc.com/public/api/'
})

export default API