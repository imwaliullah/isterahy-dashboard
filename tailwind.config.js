/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#08488C',
        'primaryLight': '#EFF6FD',
        'success': '#00B55D',
        'danger': '#F52D56',
        'darkGreen': '#00B45C',
        'darkBlack': '#2E3039',
        'darkGray': '#5C5C5C',
        'lightGray': '#9DAAB6',
      },
    },
    fontFamily: {
      'sans': ['Plus Jakarta Sans', 'sans-serif'],
      'roboto': ['Roboto', 'sans-serif']
    }
  },
  plugins: [],
}
