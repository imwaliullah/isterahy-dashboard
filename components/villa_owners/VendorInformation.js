import React from 'react'

const VendorInformation = ({ vendor }) => {
    return (
        <>
            <div className='mt-10'>
                <h4 className='text-xl text-darkBlack font-medium'>Villa Information</h4>
                <div className='mt-5'>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Focal Person Name</p>
                        <p className='text-black text-base'>{vendor?.focal_person ? vendor?.focal_person : "null"}</p>
                    </div>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Vendor ID</p>
                        <p className='text-black text-base'>{vendor?.id}</p>
                    </div>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Contact number</p>
                        <p className='text-black text-base'>{vendor?.phone ? vendor?.phone : 'null'}</p>
                    </div>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Email</p>
                        <p className='text-black text-base'>{vendor?.email ? vendor?.email : "null"}</p>
                    </div>
                    <div className='flex mb-5'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>CNIC</p>
                        <p className='text-black text-base'>{vendor?.cnic ? vendor?.cnic : "null"}</p>
                    </div>
                    <div className='flex mb-5'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Address</p>
                        <p className='text-black text-base'>{vendor?.address ? vendor?.address : "null"}</p>
                    </div>
                </div>
            </div>
            {/* <div className='flex my-8'>
                <h4 className='sm:w-[300px] text-xl text-darkBlack font-medium'>Action</h4>
                <div>
                    <div className='w-[192px] h-[35px] relative'>
                        <select
                            className='w-full h-full text-sm text-darkBlack appearance-none rounded-[25px] border border-[#94BAD4] px-5'
                            style={{ boxShadow: '0px 2px 20px #CFD9DF66' }}
                        >
                            <option selected>Pause Vendor</option>
                        </select>
                        <img src='/img/down.svg' alt="down" className='absolute right-4 top-1/2 -translate-y-1/2 w-[20px] h-[12px]' />
                    </div>
                    <p className='text-black text-xs lg:w-[370px] font-medium mt-4'>Select action and it will notify the vendor and system listing automatically.</p>
                </div>
            </div>
            <div className='flex gap-x-10 mt-12 mb-12'>
                <button
                    className='w-[176px] h-[46px] bg-success text-white font-light text-sm rounded-[14px]'
                // onClick={() => setIsOpen(true)}
                >Submit</button>
                <button className='w-[176px] h-[46px] bg-[#AEC4DB] text-white font-light text-sm rounded-[14px]'>Cancel</button>
            </div> */}
        </>
    )
}

export default VendorInformation