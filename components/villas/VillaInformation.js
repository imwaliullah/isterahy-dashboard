import React, { useEffect, useState } from 'react'

const VillaInformation = ({ data, villaImages }) => {
    const [imagesToShow, setImagesToShow] = useState([]);
    const [remainingImgLength, setRemainingImgLength] = useState([]);

    console.log(villaImages);
    useEffect(() => {
        if (villaImages) {
            const imgArr = villaImages.slice(0, 5);
            setImagesToShow(imgArr);

            const remainingLength = villaImages?.length - imgArr.length;
            setRemainingImgLength()
        }
    }, [villaImages])


    return (
        <>
            <div className='mt-10'>
                <h4 className='text-xl text-darkBlack font-medium'>Villa Information</h4>
                <div className='mt-5'>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Villa Full Busines name</p>
                        <p className='text-black text-base'>{data?.name}</p>
                    </div>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Address</p>
                        <p className='text-black text-base'>{data?.address}</p>
                    </div>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>City</p>
                        <p className='text-black text-base'>{data?.city}</p>
                    </div>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Number of Beds</p>
                        <p className='text-black text-base'>{data?.no_of_beds}</p>
                    </div>
                    <div className='flex mb-5'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Key Features</p>
                        <p className='text-black text-base'>{data?.key_features_for_female} <br /> {data?.key_features_for_general} <br /> {data?.key_features_for_male} <br /></p>
                    </div>
                    <div className='flex mb-3'>
                        <p className='text-darkGray font-light text-base sm:w-[300px]'>Description</p>
                        <p className='text-black text-base font-light lg:w-[570px]'>{data?.description}</p>
                    </div>
                </div>
            </div>
            <section className=''>
                <h4 className='text-xl text-darkBlack font-medium'>Villa Images</h4>
                <div className='flex flex-wrap gap-5 my-5'>
                    {imagesToShow?.map((item, index) => (
                        <img
                            src={item?.image}
                            alt=''
                            className='object-cover rounded-xl w-[110px] h-[110px] border border-[#B5D9FF]'
                            key={index + ""}
                        />
                    ))}

                    {remainingImgLength > 1 && (
                        <div className='rounded-xl w-[110px] h-[110px] relative overflow-hidden border-[#B5D9FF] moreImages'>
                            <img src='/img/villa1.jpg' alt='' className='object-cover w-full h-full' />
                            <span className='text-white font-medium absolute top-[calc(50%-20px)] left-[calc(50%-20px)] translate-x-1/2 translate-y-1/2'>{remainingImgLength}+</span>
                        </div>
                    )}
                </div>
                {/* <div className='flex my-8'>
                    <h4 className='sm:w-[300px] text-xl text-darkBlack font-medium'>Action</h4>
                    <div>
                        <div className='w-[192px] h-[35px] relative'>
                            <select
                                className='w-full h-full text-sm text-darkBlack appearance-none rounded-[25px] border border-[#94BAD4] px-5'
                                style={{ boxShadow: '0px 2px 20px #CFD9DF66' }}
                            >
                                <option selected>Pause Villa</option>
                            </select>
                            <img src='/img/down.svg' alt="down" className='absolute right-4 top-1/2 -translate-y-1/2 w-[20px] h-[12px]' />
                        </div>
                        <p className='text-black text-xs lg:w-[370px] font-medium mt-4'>Select action and it will notify the vendor and system listing automatically.</p>
                    </div>
                </div> */}
                <button
                    className='w-[176px] h-[46px] mt-6 bg-primary text-white font-light text-sm rounded-[14px]'
                >Approve Villa</button>
            </section>
        </>
    )
}

export default VillaInformation